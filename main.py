import csv
import redis
from nltk.stem.snowball import SnowballStemmer
import nltk
from pattern.es import parse as parse_es , split
import re
from datetime import date, timedelta , datetime
from unidecode import unidecode
from pattern.en import suggest
csv.field_size_limit(300000)

def preprocess(document , stoplist):
    return [ re.sub('["#$%&()*+,_.!?@/:;<=>\^{}~]', '',token) for token in  document.lower().split() if not ( token.startswith('http') or token.startswith('pic.') or token.startswith('www') or token in stoplist or len(token)<5)]
   
r = redis.StrictRedis(host='localhost', port=6379, db=0)
r.flushall()
stopwords = [ unidecode(word.decode('utf-8')) for word in nltk.corpus.stopwords.words('spanish')]

parties = ['aes' , 'an' , 'dn' , 'edm' , 'falange'  , 'msr' , 'np' , 'pp' , 'pxc']

prev_text = 0
for party in parties:
  filename = "data/" + party + ".tab"
  print filename
  with open(filename, 'rU') as csvfile:
      spamreader = csv.reader(csvfile, dialect=csv.excel_tab)
      i = 0
      for row in spamreader:
        i+=1
        if i>1 and len(row) > 8 and row[3] and row[8]:
          print i
          tokenised_text = []
          text = row[2]
          if text != prev_text:
            text = text.decode('utf-8' , 'ignore')
            text = unidecode(text)
            tokenised_text = preprocess(text,stopwords)
            date = datetime.strptime(row[3][0:10],'%Y-%m-%d').strftime('%y%m')
            for x1, term in enumerate(tokenised_text):
              start = max(x1-2,0)
              end = min(len(tokenised_text),x1+2)+1
              for x2,cooc in enumerate(tokenised_text[start:end]):
                distance = str(start + x2 - x1)
                if distance == '0':
                  r.zincrby(party+":official:"+date+":0"  , term , 1) 
                else:
                  r.zincrby(party+":official:"+date+":" + distance  , term + " " + cooc , 1) 
          prev_text = text
          
          ## for official posts
          tokenised_text = []
          text = row[7]
          text = text.decode('utf-8', 'ignore')
          text = unidecode(text)
          tokenised_text = preprocess(text,stopwords)
          date = datetime.strptime(row[8][0:10],'%Y-%m-%d').strftime('%y%m')
          for x1, term in enumerate(tokenised_text):
            start = max(x1-2,0)
            end = min(len(tokenised_text),x1+2)+1
            for x2,cooc in enumerate(tokenised_text[start:end]):
              distance = str(start + x2 - x1)
              if distance == '0':
                r.zincrby(party+":comments:"+date+":0"  , term , 1) 
              else:
                r.zincrby(party+":comments:"+date+":" + distance  , term + " " + cooc , 1) 


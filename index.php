<?php set_time_limit(2000);

require 'Predis/Autoloader.php';
Predis\Autoloader::register();
$client = new Predis\Client();

ini_set('memory_limit', '2000M');
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(array_key_exists('party',$_GET)){
	$party = $_GET['party'];
}
else{
	$party = "*";
}

if(array_key_exists('source',$_GET)){
	$source = $_GET['source'];
}
else{
	$source = "*";
}

if(array_key_exists('date',$_GET)){
	$date = $_GET['date'];
}
else{
	$date = "*";
}


function get_cluster($term, $CLUSTERS){
	foreach($CLUSTERS as $key=>$cluster){
		if(in_array( $term , $cluster )){
			return $key;
		}
	}
	return False;
}

$OLD_CLUSTERS = array(
	"españa"=>array( "españa", "espana", "españoles", "espanoles", "espanolas", "espanyols", "espanya", "espanyoles"),
    "pov"=>array("manifestar", "concentrar", "manifestación", "concentración", "llamada", "convocar", "llamamiento"),	
    "immigration"=>array("inmigración" , "immigració", "immigracio", "immigrants", "immigrant","inmigracion", "imigracion", "extranjeros", "extranjero", "estranjeros", "estrangero", "extrangeros", "extrangero", "estrangers", "estranger", "extrangers", "inmigrantes" , "inmigrante", "immigrante", "immigrantes"),
    "muslims"=> array("musulmán", "musulma", "musulmans", "musulmà", "musulmanes"),
    "islam"=> array("islam", "islamistas", "islámica", "islamica", "islamistes", "islamització", "islamización"),
    "pakistanis"=> array("paquistanís", "pakis", "paquistanis", "paquistanesos", "paquistanès"),
    "moroccans"=> array( "magrebís", "magrebies", "magrebí", "magrebins", "magrebí", "moro", "moros", "marroquí", "marroquíes", "marroquins", "marroquina", "marroquines"),
    "black_people"=> array("negros", "negres", "negro", "subsaharianos", "africano", "africans", "negre", "africanos", "africà", "subsahariano", "subsaharià", "negratas", "negrata"),
    "romanians"=> array("rumanos", "rumano", "romanesos", "romanès", "Rumanía"),
    "jews"=> array("judíos", "judios", "sionista", "sionismo", "judío", "jueu", "jueus", "jueva", "sionistes","judío"),
    "South_Americans"=> array("panchitos", "panchito", "sudacas", "sudaques", "sudaca", "sudamericano", "sudamericanos"),
    "ETA"=> array("etarras" , "eta" , "etarra", "etarres"),
    "terrorists"=> array("terroristas", "terroristes", "terrorisme", "terrorista","terrorismo"),
    "independentismo"=> array("independentismo", "independentistes", "independentisme", "independència", "independentisme", "independentistas", "independencia", "separatismo", "separatistas"),
    "Catalan"=> array("catalanes", "catalans", "catalán", "catalan", "catalufos", "catalufo"),
    "To_hit"=> array("pegar", "quemar", "cremar", "quemarles", "pegarles", "patear", "linchar", "lincharles", "lincharemos", "linchamiento", "lincharles", "paliza", "pegarlos", "linxar", "assaltar"),
    "Insults"=> array("mierda", "merda", "putos", "puto", "asqueroso", "asquerosos", "hijoputa", "hijoputas", "apestosos", "apestoso", "parásitos", "salvajes", "parásito", "parasitos", "parasito", "gentuza", "rojos", "cerdo", "cerdos", "chusma", "marxista", "marxistas"),
    "To_kill"=> array("matar", "asesinar", "asesinarles", "asesinarlos", "exterminarles", "exterminarlos", "matadlos", "matarlos", "matarle", "matarles", "mataremos", "exterminio", "exterminar", "muera", "mueran", "muerte", "morin", "morir"),
    "Work"=> array("trabajo" , "treballar", "treball", "treballadors", "trabajos", "trabajan", "trabajar" , "trabajador", "trabajadores", "trabajadora", "trabajadoras"),
    "Unemployment"=> array("paro" , "aturats", "atur", "desempleats", "parado" , "parados", "desempleados", "desempleado")
);

$dict = array();

for ($i=-2; $i<3; $i++){

$redis_key = $party.":".$source.":".$date."*:".$i;
$results = $client->keys($redis_key);
$size = sizeof($results);
$client->zunionstore("test",$results);
$bigrams = $client->zrevrange("test",0,-1,"WITHSCORES");
if($i != 0 ){
	
	foreach($bigrams as $bigram){
		$parts = explode(" ",$bigram[0]);
		$cluster = get_cluster($parts[0],$OLD_CLUSTERS);
		if ($cluster){
			if(array_key_exists($cluster,$dict)){
				if(array_key_exists($i,$dict[$cluster] ) ){
					if(array_key_exists($parts[1],$dict[$cluster][$i]) ){
						$dict[$cluster][$i][$parts[1]] += $bigram[1];
					}
					else{
						$dict[$cluster][$i][$parts[1]] = $bigram[1];
					}
				}
				else{
					$dict[$cluster][$i] = array();
					$dict[$cluster][$i][$parts[1]] = $bigram[1];
				}
			}
			else{
				$dict[$cluster] = array();
				$dict[$cluster][$i] = array();
				$dict[$cluster][$i][$parts[1]] = $bigram[1];
			}
		}

	}
}
else{
	$terms = $bigrams;
	foreach($terms as $term){
		$cluster = get_cluster($term[0],$OLD_CLUSTERS);
		if ($cluster){
			if(array_key_exists($cluster,$dict)){
				if(array_key_exists("total",$dict[$cluster] ) ){
					$dict[$cluster]["total"] += $term[1];
				}
				else{
					$dict[$cluster]["total"] = $term[1];
				}
			}
			else{
				$dict[$cluster] = array();
				$dict[$cluster]["total"] = $term[1];
			}
		}
	}
}
}


// var_dump($dict);

// $redis_key = "*:".$platform.":".$country.":".$party.":".$source.":".$hashtag.":0";
// $results = $client->keys($redis_key);
// $client->zunionstore("test2",$results);
// $terms = $client->zrevrange("test2",0,-1,"WITHSCORES");

// var_dump($terms);

//MAYBE save another list of ngram for every n from 1 to 6 and make them fade away according to their  maximum distance from the term.
foreach($terms as $term){
	$cluster = get_cluster($term[0],$OLD_CLUSTERS);
	if($cluster){
		echo '<div style="clear: left; padding-top:20px;">';
		echo $cluster."  [". $dict[$cluster]["total"]."]  <br>" ; 
		foreach($dict[$cluster] as $key=>$distance){
			if($key != "total"){
				echo '<div style="float: left; width: 200px;">';
				echo "&nbsp&nbsp&nbsp&nbsp" .$key ."<br>";
				foreach( array_slice($dict[$cluster][$key] , 0 , 10) as $coloc_key=>$freq){
					echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" .$coloc_key ."  ". $freq ."<br>";
				}
				echo "</div>";
			}
		}
	}
	
	

	
	// foreach($dict[$item[0]] as $coloc){
	// 	echo"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . $coloc."   ".$coloc["fake_freq"]."] <br> " ;
	// }
	// }
	// echo "hello";
}

// $json_string = json_encode($dict, JSON_PRETTY_PRINT);
// echo $json_string;


?>
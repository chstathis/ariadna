README
======
This is a small script for saving coocordance distribution of a corpus of facebook posts on a redis database, and then viewing them as they are , or organised as clusters. For every facebook post or comment the script normalises the text and removes spanish stopwords , and after that it iterates through the words and saves a count for every cooncordance along with the distance of that cooncordance and other metadata like "political party" , "official" or "comment" , and the month of the year it was posted. The distributions are organised as sorted sets in a Redis server so that they can be easily manipulated later.

INSTALLATION
============

1.Install Redis

	http://redis.io/

2.Install Python and pip

	https://www.python.org/

	https://pypi.python.org/pypi/pip

3.Install dependecies

	NLTK - http://www.nltk.org/ (and download stemmers and stopwords corpus)
	redispy - http://redis-py.readthedocs.org/en/latest/
	pattern-es - http://www.clips.ua.ac.be/pages/pattern-es (this was used for POS tagging tests)
	unidecode - https://pypi.python.org/pypi/Unidecode


RESULTS
=======
After running the script succesfuly you can view the results by using one of the index-.php files accordingly. It has to be somewhere inside the MAMP root folder (or some other server) and you need Predis as a client for the Redis Database.

	https://github.com/nrk/predis
	http://www.mamp.info/en/